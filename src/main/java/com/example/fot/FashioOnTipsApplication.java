package com.example.fot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FashioOnTipsApplication {

	public static void main(String[] args) {
		SpringApplication.run(FashioOnTipsApplication.class, args);
	}

}
